## sm6150-user 10 QKQ1.190915.002 eng.root.20210413.105619 release-keys
- Manufacturer: oppo
- Platform: sm6150
- Codename: OP4B83L1
- Brand: OPPO
- Flavor: sm6150-user
- Release Version: 10
- Id: QKQ1.190915.002
- Incremental: eng.root.20210413.105619
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: CPH1907PUEX_11.C.49_1490_202104131023
- Branch: CPH1907PUEX_11.C.49_1490_202104131023
- Repo: oppo_op4b83l1_dump_14025


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
