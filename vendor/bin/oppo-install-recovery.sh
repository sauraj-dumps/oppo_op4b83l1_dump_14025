#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:100663296:f6eabf90e136a20d1062a31e0faa3bf480a5c9c5; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:9ef0191ec6c1dc752b64318538163db9b5ddda77 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:100663296:f6eabf90e136a20d1062a31e0faa3bf480a5c9c5 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.recovery.updated true
fi
